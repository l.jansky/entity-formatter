import moment from 'moment';

export const date = input => {
	if (!input) {
		return input;
	}

	const output = new Date(input);
	output.setMinutes(output.getMinutes() - output.getTimezoneOffset());

	return output.toJSON().slice(0, 10);
};

export const dateTime = input => {
	if (!input) {
		return input;
	}

	return moment(input).format('YYYY-MM-DD HH:mm:ss');
};
