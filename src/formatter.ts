import R from 'ramda';
export { default as parseFunction } from './parse-function';
import parseFunction from './parse-function';
import * as formatFunctions from './format-functions';
import {
	FormatObject,
	Format,
	Pair,
	Relation,
	parseFormat,
	EntityData
} from './formatter.types';
export * from './formatter.types';

const isObject = (val: any) => R.type(val) === 'Object';
const isArray = (val: any) => R.type(val) === 'Array';

const mergeParams = (format: FormatObject) =>
	R.pipe<string[], FormatObject[], FormatObject[], FormatObject>(
		R.map((param: string) => parse(param, false)),
		R.prepend(format),
		R.mergeAll
	);

export const parse: parseFormat = (
	formatString: string,
	init = true,
	format: FormatObject = {}
): FormatObject => {
	const parsed = parseFunction(init ? `(${formatString})` : formatString);

	if (init) {
		const r = mergeParams(format)(parsed.params);
		return r;
	} else {
		if (!parsed.params.length) {
			const path = parsed.name.split('.');
			const star = path[path.length - 1] === '*';
			const lensPath = R.lensPath(path.filter(item => item !== '*'));
			return R.set(lensPath, star ? {} : 1, {});
		} else {
			if (parsed.name !== '') {
				const [name] = parsed.params;
				return { [name]: parsed.name };
			} else {
				const [name, ...params] = parsed.params;
				const r = mergeParams(format)(params);
				return { [name]: r };
			}
		}
	}
};

export const getRelations = R.pipe<Format, Format, Pair<Format>[], Relation[]>(
	R.pickBy(isObject),
	R.toPairs,
	R.reduce((acc, pair) => {
		const actualItem: Relation = { name: pair[0] };
		const relations = getRelations(pair[1]);
		if (relations.length > 0) {
			actualItem.relations = relations;
		}

		return R.concat(acc, [actualItem]);
	}, [])
);

const formatOneBase = (
	format: FormatObject,
	entity: EntityData
): EntityData => {
	const ret: EntityData =
		R.keys(format).length === 0
			? R.pickBy(attr => !isObject(attr), entity)
			: {};

	for (let key in format) {
		if (format[key] === 1) {
			ret[key] = entity[key];
		} else if (typeof format[key] === 'string') {
			const formatFunction = format[key] as string;
			if (formatFunctions[formatFunction]) {
				ret[key] = formatFunctions[formatFunction](entity[key]);
			}
		} else if (isObject(format[key])) {
			const formatObject = format[key] as FormatObject;
			if (isObject(entity[key])) {
				ret[key] = formatOne(formatObject, entity[key] as EntityData);
			}

			if (isArray(entity[key])) {
				ret[key] = R.map<EntityData, EntityData>(
					item => formatOneBase(formatObject, item),
					entity[key] as EntityData[]
				);
			}
		}
	}

	return ret;
};

export const formatOne = R.curry(formatOneBase);
