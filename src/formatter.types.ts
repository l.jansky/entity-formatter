import parseFunction from './parse-function';

export type Format = FormatObject | number | string;

export interface FormatObject {
	[key: string]: Format;
}

export type Pair<T> = [string, T];

export interface Relation {
	name: string;
	relations?: Relation[];
}

export interface EntityData {
	[key: string]: string | number | EntityData | EntityData[];
}

export interface ParsedFunction {
	name: string;
	params: string[];
}

export type parseFormat = (
	formatString: string,
	init?: boolean,
	format?: FormatObject
) => FormatObject;

export type formatOne = (
	format: FormatObject,
	entity: EntityData
) => EntityData;
export type formatOneCurried = (
	format: FormatObject
) => (entity: EntityData) => EntityData;

export interface Formatter {
	parse: parseFormat;
	getRelations: (format: Format) => Relation[];
	formatOne: formatOne | formatOneCurried;
}
