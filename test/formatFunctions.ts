import chai from 'chai';
const assert = chai.assert;
import { dateTime } from '../src/format-functions';

describe('Format functions', () => {
	it.skip('should format dateTime', () => {
		const input = '2019-03-26T18:00:00.000Z';
		const output = dateTime(input);
		assert.equal(output, '2019-03-26 19:00:00');
	});
});
